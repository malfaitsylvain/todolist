<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    /**
     * @Route("/user", name="user")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createUserAction(Request $request)
    {
        $user = new User();

        $formBuilder = $this->createFormBuilder($user, [
            'action' => $this->generateUrl('user')
        ]);

        $formBuilder
            ->add('username', null, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Username'
                ]
            ])
            ->add('email', null, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Email address'
                ]
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => true,
                'invalid_message' => 'The password fields must match.',
                'first_options' => [
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'Password'
                    ]
                ],
                'second_options' => [
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'Password validation'
                    ]
                ]
            ])
            ->add('submit', SubmitType::class);

        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setEnabled(true);
            $user->addRole('ROLE_ADMIN');

            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);

            $this->addFlash('notice', 'User ' . $user->getUsername() . ' successfully created');
        }

        return $this->render('default/user.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
