<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Todo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     *
     * @return Response
     */
    public function indexAction()
    {
        $user = $this->getUser();
        $repository = $this->getDoctrine()->getRepository('AppBundle:Todo');

        $todos = $repository->findBy(['user' => $user]);

        return $this->render('default/index.html.twig', [
            'todos' => $todos,
        ]);
    }

    /**
     * @Route("/add", name="add")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addAction(Request $request)
    {
        $user = $this->getUser();
        $value = $request->request->get('value');

        $todo = new Todo();
        $todo->setUser($user);
        $todo->setValue($value);

        $em = $this->getDoctrine()->getManager();

        $em->persist($todo);
        $em->flush();

        $view = $this->renderView('default/block/todo.html.twig', [
            'todo' => $todo
        ]);

        return new JsonResponse(['id' => $todo->getId(), 'view' => $view]);
    }

    /**
     * @Route("/remove", name="remove")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removeAction(Request $request)
    {
        $id = $request->request->get('id');

        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository('AppBundle:Todo');

        $todo = $repository->find($id);

        if (null !== $todo) {
            $em->remove($todo);
            $em->flush();
        }

        return new JsonResponse();
    }
}
