- Requires PHP extension pdo_sqlite (a sqlite file is used as the application database)

- /!\ The server must have a write permission on the app/databases directory and the sqlite file under it

- To access the application a user must first be created (e.g. `bin/console fos:user:create admin email password --super-admin`)

- The application was directly run from an Apache2 server without using any Docker environment